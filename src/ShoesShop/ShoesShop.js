import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemShoes from '../component/ItemShoes';
import ListShoes from '../component/ListShoes';

class ShoesShop extends Component {

    renderItemShoes = () => {
        return this.props.shoesData.map(item => {
            return (
                <ItemShoes
                    key={item.id}
                    shoesData={item}
                />
            )
        })
    }

    render() {
        return (
            <div>
                <ListShoes
                    renderItemShoes={this.renderItemShoes()}
                />
            </div>
        );
    }
}


let mapStateToProps = (state) => {
    return ({
        shoesData: state.shoesShopReducer.shoesData,
    })
}

export default connect(mapStateToProps)(ShoesShop);