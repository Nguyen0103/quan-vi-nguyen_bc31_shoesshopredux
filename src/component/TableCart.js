import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CHANGE_QUANTITY, REMOVE_SHOES } from '../redux/types/shoesShopType'

class TableCart extends Component {

    renderTableCart = () => {
        return this.props.cart.map(item => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.changeQuantity(item.id, 1)
                            }}
                            className='btn btn-success'>+</button>
                        <span className='mx-2'>{item.quantity}</span>
                        <button
                            onClick={() => {
                                this.props.changeQuantity(item.id, -1)
                            }}
                            className='btn btn-warning'>-</button>
                    </td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.removeShoes(item.id)
                            }}
                            className='btn btn-danger'>delete</button>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr >
                            <th>Id</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTableCart()}
                    </tbody>
                </table>
            </div>
        )
    }
}


let mapStateToProps = (state) => {
    return ({
        cart: state.shoesShopReducer.cart,
    })
}

let mapDispatchToProps = (dispatch) => {
    return ({
        removeShoes: (id) => {
            dispatch({
                type: REMOVE_SHOES,
                payload: id,
            })
        },
        changeQuantity: (id, step) => {
            dispatch({
                type: CHANGE_QUANTITY,
                payload: { id, step },
            })
        }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(TableCart)
