import React, { Component } from 'react';
import TableCart from './TableCart';

class ListShoes extends Component {
    render() {
        return (
            <div>
                <TableCart />
                <div className="row">{this.props.renderItemShoes}</div>
            </div>
        );
    }
}

export default ListShoes;