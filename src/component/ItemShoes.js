import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ADD_SHOES } from '../redux/types/shoesShopType'

class ItemShoes extends Component {
    render() {
        let { image, name, description } = this.props.shoesData
        return (
            <div className='col-3'>
                <div className="card" style={{ width: '100%' }}>
                    <img src={image} className="card-img-top" />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">
                            {description.length < 30
                                ? description
                                : description.slice(0.3) + '...'}
                        </p>
                        <button
                            onClick={() => {
                                this.props.addnewShoes(this.props.shoesData)
                            }}
                            className="btn btn-primary">
                            Add to cart
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}


let mapDispatchToProps = (dispatch) => {
    return ({
        addnewShoes: (product) => {
            dispatch({
                type: ADD_SHOES,
                payload: product,
            })
        }
    })
}

export default connect(null, mapDispatchToProps)(ItemShoes)