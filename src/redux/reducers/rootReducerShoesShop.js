import { combineReducers } from "redux";
import { shoesShopReducer } from "./shoesShopReducer";

export const rootReducer = combineReducers({
    shoesShopReducer,
})