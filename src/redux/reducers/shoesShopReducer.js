import { shoesData } from "../../assets/shoesData"
import { ADD_SHOES, REMOVE_SHOES, CHANGE_QUANTITY } from "../types/shoesShopType"


let initialState = {
    shoesData: shoesData,
    cart: [],
}

export let shoesShopReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case ADD_SHOES: {
            let index = state.cart.findIndex(item => {
                return (item.id === payload.id)
            })
            let updateCart = [...state.cart];
            if (index === -1) {
                let newOroduct = { ...payload, quantity: 1 }
                updateCart.push(newOroduct)
            } else {
                updateCart[index].quantity++;
            }
            state.cart = updateCart;
            return ({ ...state })
        }

        case REMOVE_SHOES: {
            return ({ ...state, cart: state.cart.filter(item => item.id !== payload) })
        }

        case CHANGE_QUANTITY: {
            let index = state.cart.findIndex(item => {
                return (item.id === payload.id)
            })
            let updateCart = [...state.cart]
            updateCart[index].quantity += payload.step;
            if (updateCart[index].quantity === 0) {
                updateCart.splice(index, 1)
            }
            return ({ ...state, cart: updateCart })
        }

        default:
            return ({ ...state })
    }
}
